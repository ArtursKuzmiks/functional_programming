package tasks.third

/**
 * @author Arturs Kuzmiks on 07/10/2019.
 */

/**
 * We define a set by its characteristic function
 */
typealias Set = (Int) -> Boolean

/**
 * NB!: You're allowed to use self-implemented Set functions as well
 * as `forAll()` in other function implementations
 */

/**
 * If  a set contains an element.
 */
infix fun Set.contains(elem: Int): Boolean = invoke(elem)

/**
 * Singleton set from one element
 */
fun singletonSet(elem: Int): Set = { it == elem }

/**
 * Union of the two sets
 */
infix fun Set.union(set: Set): Set = { contains(it) or set.contains(it) }

/**
 * Intersection of the two sets
 */
infix fun Set.intersect(set: Set): Set = { contains(it) and set.contains(it) }

/**
 * Difference of two given sets
 */
infix fun Set.diff(set: Set): Set = { contains(it) and set.contains(it).not() }

/**
 * Filter
 */
fun Set.filter(predicate: (Int) -> Boolean): Set = { contains(it) and predicate(it) }

/** =========== For brave enough =========== */

const val BOUND = 1000

/**
 * if  all bounded integers within s satisfy p
 */
fun Set.forAll(predicate: (Int) -> Boolean): Boolean {
    tailrec fun iterate(a: Int): Boolean =
            when {
                a > BOUND -> true
                contains(a) and predicate(a).not() -> false
                else -> iterate(a + 1)
            }
    return iterate(-BOUND)
}

infix fun Set.exists(predicate: (Int) -> Boolean): Boolean = filter(predicate).forAll { false }.not()

fun Set.map(transform: (Int) -> Int): Set = { x -> exists { transform(it) == x } }

val Set.contents: String
    get() = (-BOUND..BOUND).filter { contains(it) }.joinToString(prefix = "{", postfix = "}", separator = ",")