package tasks.second;

/**
 * @author Arturs Kuzmiks on 20/09/2019.
 */
public class Solution {

    static boolean palindrome(String srt) {
        srt = srt.toLowerCase();
        return (srt.isEmpty() || srt.length() == 1) ||
                (srt.charAt(0) == srt.charAt(srt.length() - 1) && palindrome(srt.substring(1, srt.length() - 1)));
    }

    static long superDigit(long number) {
        return number == 0 ? 0 : number % 10 + superDigit(number / 10);
    }

    static int powerSum(int x, int n) {
        int start = 1;
        return powerSum(x, n, start);
    }

    private static int powerSum(int x, int n, int number) {
        int val = (int) Math.pow(number, n);
        if (val == x)
            return 1;
        else if (val < x)
            return powerSum(x, n, number + 1) + powerSum(x - val, n, number + 1);
        else
            return 0;
    }
}
