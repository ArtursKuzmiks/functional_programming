package tasks.first;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * @author Arturs Kuzmiks on 04/09/2019.
 */
public class PerfectNumber {

    private PerfectNumber() {
    }

    static STATE detect(int number) {
        return process(number);
    }

    static Set<Integer> divisors(int number) {
        return IntStream.rangeClosed(1, (int) Math.sqrt(number))
                .filter(n -> number % n == 0)
                .flatMap(n -> IntStream.of(n, number / n))
                .boxed()
                .collect(Collectors.toSet());
    }

    static STATE process(int number) {
        int sum = divisors(number).stream()
                .mapToInt(Integer::intValue)
                .filter(n -> number != n)
                .sum();

        return STATE.getClassification(sum, number);
    }
}
