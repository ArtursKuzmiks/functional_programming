package tasks.first;

/**
 * @author Arturs Kuzmiks on 04/09/2019.
 */
public enum STATE {
    PERFECT, DEFICIENT, ABUNDANT;

    STATE() {
    }

    static STATE getClassification(int sum, int number) {
        if (sum == number) {
            return PERFECT;
        } else if (sum < number) {
            return DEFICIENT;
        } else {
            return ABUNDANT;
        }
    }
}
