package tasks.second;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author Arturs Kuzmiks on 20/09/2019.
 */
public class TestSolution {

    @Test
    public void superDigit1() {
        long number = 9875;
        long sum = 29;
        assertEquals(sum, Solution.superDigit(number));
    }

    @Test
    public void superDigit2() {
        long number = 11111111;
        long sum = 8;
        assertEquals(sum, Solution.superDigit(number));
    }

    @Test
    public void superDigit3() {
        long number = 11111111, sum = 9;
        assertNotEquals(sum, Solution.superDigit(number));
    }

    @Test
    public void palindrome1() {
        String str = "";
        assertTrue(Solution.palindrome(str));
    }

    @Test
    public void palindrome2() {
        String str = "s";
        assertTrue(Solution.palindrome(str));
    }

    @Test
    public void palindrome3() {
        String str = "Sapalsarītadēdatīraslapas";
        assertTrue(Solution.palindrome(str));
    }

    @Test
    public void palindrome4() {
        String str = "sa";
        assertFalse(Solution.palindrome(str));
    }

    @Test
    public void powerSum1() {
        int x = 13, n = 2, sum = 1;
        assertEquals(sum, Solution.powerSum(x, n));
    }

    @Test
    public void powerSum2() {
        int x = 100, n = 2, sum = 3;
        assertEquals(sum, Solution.powerSum(x, n));
    }
}
