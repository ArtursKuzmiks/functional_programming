package tasks.first;


import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

/**
 * @author Arturs Kuzmiks on 04/09/2019.
 */

public class PerfectNumberTest {

    @Test
    public void test6Perfect() {
        assertEquals(STATE.PERFECT, PerfectNumber.process(6));
    }

    @Test
    public void test8Deficient() {
        assertEquals(STATE.DEFICIENT, PerfectNumber.process(8));
    }

    @Test
    public void test20Abundant() {
        assertEquals(STATE.ABUNDANT, PerfectNumber.process(20));
    }

    @Test
    public void test16DeficientWithIntSqrt() {
        assertEquals(STATE.DEFICIENT, PerfectNumber.process(16));
    }

    @Test
    public void test1Deficient() {
        assertEquals(STATE.DEFICIENT, PerfectNumber.process(1));
    }

    @Test
    public void testDivisors1() {
        Object[] expected = new Integer[]{1};
        int n = 1;
        assertArrayEquals(expected, PerfectNumber.divisors(n).toArray());
    }

    @Test
    public void testDivisors6() {
        Object[] expected = new Integer[]{1, 2, 3, 6};
        int n = 6;
        assertArrayEquals(expected, PerfectNumber.divisors(n).toArray());
    }

    @Test
    public void testDivisors8() {
        Object[] expected = new Integer[]{1, 2, 4, 8};
        int n = 8;
        assertArrayEquals(expected, PerfectNumber.divisors(n).toArray());
    }

    /**
     * Detect method test.
     * {@link PerfectNumber#detect(int)}
     */
    @Test
    public void testDetect() {
        assertEquals(STATE.PERFECT, PerfectNumber.detect(28));
        assertEquals(STATE.DEFICIENT, PerfectNumber.detect(1));
        assertEquals(STATE.ABUNDANT, PerfectNumber.detect(20));
    }


}

